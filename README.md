## Synopsis

This is a lucee based web app to control Meridian Audio equipment.
The interface is stolen shamelessly from an Isotope demo https://isotope.metafizzy.co/.
As its a web app, it'll work on phones, ipads etc.
I've tried it on an old ipad (5.x) and ipod touch, and aside from the isotope filter buttons, it works just fine.

## Installation

Requires Lucee http://lucee.org/ to be installed.
Should also work with commandbox.

I use it with a global Cache GC-100 to talk RS232 to the Meridian box.
Also tried with a USB serial cable and ser2net on OSX, but the GC-100 is much more reliable.

## Tests

Tested on Mac OSX (mostly) and Windows.
I use it daily with a 565 and 501.

## License

MIT, Isotope is GPLv3.
