
<html>
<head>
<script src="jquery-1.10.1.min.js"></script>
<script src="isotope-pkgd-min.js"></script>
<script src="knob-min.js"></script>

</head>
<body>

<!--- Read a config file... --->
<cfscript>
 
  param name="SESSION.host" default="";
  param name="SESSION.port" default="";

  myFile = expandPath( "host.conf" );
  fileObj = fileOpen( myFile, "read" );

  while( NOT fileIsEOF( fileObj ) ){
    
    line = fileReadLine( fileObj );
    
    if (GetToken(line, 1, "=") EQ 'host'){
      SESSION.host = GetToken(line, 2, "=");
    }
    if (GetToken(line, 1, "=") EQ 'port'){
      SESSION.port = GetToken(line, 2, "=");
    }
  
    //WriteOutput( "#line#<br />" );
  }
  FileClose( fileObj );


 // writedump(SESSION);
</cfscript>

<style>
* { box-sizing: border-box; }

body {
  font-family: sans-serif;
  background-color: black;
}

.input.input-field,
.tel-number-field,
.textarea-field,
.select-field{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border: 1px solid #C2C2C2;
    box-shadow: 1px 1px 4px #EBEBEB;
    -moz-box-shadow: 1px 1px 4px #EBEBEB;
    -webkit-box-shadow: 1px 1px 4px #EBEBEB;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    padding: 7px;
    outline: none;
}

.input-field:focus,
.select-field:focus{
    border: 1px solid #0C0;
}

.mybutton{
    border: none;
    padding: 8px 15px 8px 15px;
    background: #FF8500;
    color: #fff;
    box-shadow: 1px 1px 4px #DADADA;
    -moz-box-shadow: 1px 1px 4px #DADADA;
    -webkit-box-shadow: 1px 1px 4px #DADADA;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
}

.overlay{
  color:#00FF00;
  position:absolute;
  top:20px;
  left:20px;
  font-size:60px;
  pointer-events:none;
}

/* ---- button ---- */

.button {
  display: inline-block;
  padding: 0.5em 1.0em;
  background: #EEE;
  border: none;
  border-radius: 7px;
  background-image: linear-gradient( to bottom, hsla(0, 0%, 0%, 0), hsla(0, 0%, 0%, 0.2) );
  color: #222;
  font-family: sans-serif;
  font-size: 16px;
  text-shadow: 0 1px white;
  cursor: pointer;
}

.button:hover {
  background-color: #8CF;
  text-shadow: 0 1px hsla(0, 0%, 100%, 0.5);
  color: #222;
}

.button:active,
.button.is-checked {
  background-color: #28F;
}

.button.is-checked {
  color: white;
  text-shadow: 0 -1px hsla(0, 0%, 0%, 0.8);
}

.button:active {
  box-shadow: inset 0 1px 10px hsla(0, 0%, 0%, 0.8);
}

/* ---- button-group ---- */

.button-group {
  margin-bottom: 20px;
}

.button-group:after {
  content: '';
  display: block;
  clear: both;
}

.button-group .button {
  float: left;
  border-radius: 0;
  margin-left: 0;
  margin-right: 1px;
}

.button-group .button:first-child { border-radius: 0.5em 0 0 0.5em; }
.button-group .button:last-child { border-radius: 0 0.5em 0.5em 0; }

/* ---- isotope ---- */

.grid {
   border: 1px solid #333;
}

/* clear fix */
.grid:after {
  content: '';
  display: block;
  clear: both;
}

/* ---- .element-item ---- */

.element-item {
  position: relative;
  float: left;
  width: 100px;
  height: 100px;
  margin: 5px;
  padding: 10px;
  background: #888;
  color: #262524;
}

.element-item > * {
  margin: 0;
  padding: 0;
}

.element-item .name {
  position: absolute;

  left: 10px;
  top: 60px;
  text-transform: none;
  letter-spacing: 0;
  font-size: 12px;
  font-weight: normal;
}

.element-item .symbol {
  position: absolute;
  left: 10px;
  top: 0px;
  font-size: 42px;
  font-weight: bold;
  color: white;
}

.element-item .number {
  position: absolute;
  right: 8px;
  top: 5px;
}

.element-item .weight {
  position: absolute;
  left: 10px;
  top: 76px;
  font-size: 12px;
}

.element-item.alkali          { background: #F00; background: hsl(   0, 100%, 50%); }
.element-item.alkaline-earth  { background: #F80; background: hsl(  36, 100%, 50%); }
.element-item.lanthanoid      { background: #FF0; background: hsl(  72, 100%, 50%); }
.element-item.actinoid        { background: #0F0; background: hsl( 108, 100%, 50%); }
.element-item.transition      { background: #0F8; background: hsl( 144, 100%, 50%); }
.element-item.post-transition { background: #0FF; background: hsl( 180, 100%, 50%); }
.element-item.metalloid       { background: #08F; background: hsl( 216, 100%, 50%); }
.element-item.diatomic        { background: #00F; background: hsl( 252, 100%, 50%); }
.element-item.halogen         { background: #F0F; background: hsl( 288, 100%, 50%); }
.element-item.noble-gas       { background: #F08; background: hsl( 324, 100%, 50%); }

.player img{
position:absolute;
top:50%;
left:50%;
margin-top:-50px;
margin-left:-50px;
}


</style>




<!--- Ze Display... --->
<div class="overlay">

<div id="device-reply" ></div>

</div>

<div style="float:right;color:white;">
  <!--- IP address and port... --->
  <form name="host_form" id="host_form" method="" action="">
    <cfoutput>
    <label for="host">Ethernet to Serial device IP</label> <input type="text" name="host" value="#SESSION.host#" placeholder="yo.ur.ip.address" class="input input-field" style="width:120px;"> 
    <label for="port">Port</label> <input type="text" name="port" value="#SESSION.port#" placeholder="4999"  class="input input-field" style="width:120px;"> 
    <input type="button" id="host_submit" name="submit"  class="mybutton" value="GO">
   </cfoutput> 
  </form>

</div>

<!--- Space, man... --->
<h1>&nbsp;</h1>
<h2>&nbsp;</h2>


<div id="filters" class="button-group">  <button class="button is-checked" data-filter="*">Show all</button>
  <button class="button" data-filter=".metalloid" id="input_filter_button">Input</button>
  <button class="button" data-filter=".volume">Volume</button>
  <button class="button" data-filter=".preset">Presets</button>
  <button class="button" data-filter=".menu">Menu</button>
</div>


<div class="grid">

<!--- Inputs... --->
  <div class="element-item transition metalloid input-selector " data-category="transition" data-value="CD" id="CD">
    <h3 class="name">CD Player</h3>
    <p class="symbol">CD</p>
  </div>
  <div class="element-item transition metalloid input-selector " data-category="transition" data-value="RD" id="RD">
    <h3 class="name">Radio</h3>
    <p class="symbol">RD</p>
  </div>
  <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="LP" id="LP">
    <h3 class="name">Record Player</h3>
    <p class="symbol">LP</p>
  </div>
   <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="TV" id="TV">
    <h3 class="name">TV</h3>
    <p class="symbol">TV</p>
  </div>
   <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="T1" id="T1">
    <h3 class="name">Tape 1</h3>
    <p class="symbol">T1</p>
  </div>
   <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="T2" id="T2">
    <h3 class="name">Tape 2</h3>
    <p class="symbol">T2</p>
  </div>
   <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="CR" id="CR">
    <h3 class="name">CDR</h3>
    <p class="symbol">CR</p>
  </div>
     <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="CB" id="CB">
    <h3 class="name">Cable</h3>
    <p class="symbol">CB</p>
  </div>
    <div class="element-item post-transition metalloid input-selector" data-category="post-transition" data-value="LD" id="LD">
    <h3 class="name">LD Input</h3>
    <p class="symbol">LD</p>
  </div>

<!--- Presets... --->
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN0" id="PN0">
    <h3 class="name">Direct</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN1" id="PN1">
    <h3 class="name">Music</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN2" id="PN2">
    <h3 class="name">Trifield</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN3" id="PN3">
    <h3 class="name">Ambisonics</h3>
    <p class="symbol">PR</p>
  </div>
   <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN4" id="PN4">
    <h3 class="name">Super</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN5" id="PN5">
    <h3 class="name">Stereo</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN6" id="PN6">
    <h3 class="name">Mulogic</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN7" id="PN7">
    <h3 class="name">Pro Logic</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN8" id="PN8">
    <h3 class="name">THX</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN11" id="PN11">
    <h3 class="name">TV Logic</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN12" id="PN12">
    <h3 class="name">AC-3</h3>
    <p class="symbol">PR</p>
  </div>
  <div class="element-item alkaline-earth metal preset input-selector" data-category="preset"  data-value="PN13" id="PN13">
    <h3 class="name">AC-3 THX</h3>
    <p class="symbol">PR</p>
  </div>


<!--- Menu... --->
  <div class="element-item noble-gas nonmetal menu input-selector" data-category="menu" data-value="ML" id="ML">
    <h3 class="name">Menu Left</h3>
    <p class="symbol">ML</p>
  </div>
  <div class="element-item noble-gas nonmetal menu input-selector" data-category="menu" data-value="MR" id="MR">
    <h3 class="name">Menu Right</h3>
    <p class="symbol">MR</p>
  </div>
  <div class="element-item noble-gas nonmetal menu input-selector" data-category="menu" data-value="MP" id="MP">
    <h3 class="name">Menu Up</h3>
    <p class="symbol">MP</p>
  </div>
  <div class="element-item noble-gas nonmetal menu input-selector" data-category="menu" data-value="MM" id="MM">
    <h3 class="name">Menu Down</h3>
    <p class="symbol">MM</p>
  </div>
  <div class="element-item noble-gas nonmetal menu input-selector" data-category="menu" data-value="DI" id="DI">
    <h3 class="name">Display</h3>
    <p class="symbol">DI</p>
  </div>

<!--- Lower vol... --->
  <div class="element-item volume actinoid metal inner-transition input-selector " data-category="volume" data-value="VN45" id="VN45">
    <h3 class="name">Volume 45</h3>
    <p class="symbol">VN</p>
  </div>

  <!--- Mute... --->
  <div class="element-item volume actinoid metal inner-transition input-selector " data-category="volume" data-value="MU" id="MU">
    <h3 class="name">Mute</h3>
    <p class="symbol">MU</p>
  </div>
<!--- Standby... --->
  <div class="element-item post-transition metal input-selector " data-category="" data-value="SB" id="SB">
    <h3 class="name">Standby</h3>
    <p class="symbol">SB</p>
  </div> 



</div>

<!--- Presets... --->
<!--- 
PN0=Direct
PN1=Music
PN2=Trifield
PN3=Ambisonics
PN4=Super
PN5=Stereo
PN6=Mulogic
PN7=Pro Logic
PN8=THX
PN9=Mono
PN10=Academy
PN11=TV Logic
PN12=AC-3
PN13=AC-3 THX 
--->

<!--- The volume control... --->
<div class="demo" style="float:right;">

    <div style="float:left;width:180px;height:320px;padding:20px;text-align:center;">
        <input class="infinite" value="0" data-width="190" data-thickness=".5" data-fgColor="#ffec03" data-bgColor="#3d3d3d" data-displayInput="false" data-cursor="true"> 
    </div>
    <div style="float:left;margin-top:165px;">
        <div class="ival" style="width:80px;text-align:center;font-size:50px;color:#AAA">45</div>
        <div class="idir" style="width:80px;text-align:center;font-size:50px;color:#AAA"></div>
    </div>
</div>


<script>
// external js: isotope.pkgd.js

$( document ).ready(function() {

// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows',
  getSortData: {
    name: '.name',
    symbol: '.symbol',
    number: '.number parseInt',
    category: '[data-category]',
    weight: function( itemElem ) {
      var weight = $( itemElem ).find('.weight').text();
      return parseFloat( weight.replace( /[\(\)]/g, '') );
    }
  }
});

// filter functions
var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};

// bind filter button click
$('.button').bind("click touchstart", function(){
//$('.button').on( 'click', function() {
  var filterValue = $( this ).attr('data-filter');
  // use filterFn if matches value
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
});

// bind sort button click
$('#sorts').on( 'click', 'button', function() {
  var sortByValue = $(this).attr('data-sort-by');
  $grid.isotope({ sortBy: sortByValue });
});

// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});



// Click an input etc...  
$('.input-selector').on( 'click', function(e) {

  var url = "ser_post.cfm";
  var thisLabel = $(this).attr('data-value');
  
  $.ajax({
        type: "POST",
        url: url,
        data: {thisLabel: thisLabel}, 
        success: function(data)
        {   
          // Update the 'display'...
          $('#device-reply').html(data);

        }
    });

});

// Host form submit...
$('#host_submit').on( 'click', function(e) {

  var url = "ser_post.cfm";
  var data = $("#host_form").serialize(); 

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(data)
        {   

        alert('Host saved.');

        }
    });

});


 // The roundy roundy control...
 $(function($) {

    $(".knob").knob({
        change : function (value) {
          // console.log("change : " + value);

        },
        release : function (value) {
            //console.log(this.$.attr('value'));
            //console.log("release : " + value);
        },
        cancel : function () {
            //console.log("cancel : ", this);
        },
        /*format : function (value) {
          return value + '%';
          },*/
        draw : function () {

            // "tron" case
            if(this.$.data('skin') == 'tron') {

                this.cursorExt = 0.3;

                var a = this.arc(this.cv)  // Arc
                        , pa                   // Previous arc
                        , r = 1;

                this.g.lineWidth = this.lineWidth;

                if (this.o.displayPrevious) {
                    pa = this.arc(this.v);
                    this.g.beginPath();
                    this.g.strokeStyle = this.pColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                    this.g.stroke();
                }

                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
            }
        }
    });

    // Example of infinite knob, iPod click wheel
    var v, up=0,down=0,i=45
            ,$idir = $("div.idir")
            ,$ival = $("div.ival")
            ,incr = function() { i++; $idir.show().html("+").fadeOut(); $ival.html(i); }
            ,decr = function() { i--; $idir.show().html("-").fadeOut(); $ival.html(i); };
    
    
    $("input.infinite").knob(
            {

        release : function (value) {

            var url = "ser_post.cfm";
            var thisLabel = 'VN' +  $ival.html();

            $.ajax({
                      type: "POST",
                      url: url,
                      data: {thisLabel: thisLabel}, 
                      success: function(data)
                      {   
                        // Update 'Display'...
                        $('#device-reply').html(data);

                      }
                  });

              },

                min : 0
                , max : 100
                , stopper : true
                , change : function () {
                if(v > this.cv){
                    if(up){
                        decr();
                        up=0;
                    }else{up=1;down=0;}
                } else {
                    if(v < this.cv){
                        if(down){
                            incr();
                            down=0;
                        }else{down=1;up=0;}
                    }
                }
                v = this.cv;
            }
            });
    });

    // So, on page load, we'll just show the inputs...
   // $( document ).ready(function() {
      $('#input_filter_button').click();
   // });

});

</script>

</body>
</html>