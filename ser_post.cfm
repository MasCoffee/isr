<cfsetting  requestTimeOut = "3">

<cfscript>

    // The host form was submitted...
    if (IsDefined("FORM.host")){

        param name="FORM.host" default="#SESSION.host#";
        param name="FORM.port" default="#SESSION.port#";

        // Write out new vals on change...
        if (FORM.host NEQ SESSION.host OR FORM.port NEQ SESSION.port){

            myFile = expandPath( "host.conf" );
            data = "host=#FORM.host##CHR(13)#port=#FORM.port##CHR(13)#";
            FileWrite(myFile, data);

        }

    } else {

        // We're sending stuff to the unit...    
        param name="FORM.thisLabel" default="TV";
        param name="result" default="";

        // Session scope is populated, all good...
        if (StructKeyExists(SESSION, 'host')){

            result =  easySocket('#SESSION.host#','#SESSION.port#', FORM.thisLabel);
            writeoutput(result);
            flush;

        } else {

            // Try and read the host file - session may have timed out...
            myFile = expandPath( "host.conf" );
            fileObj = fileOpen( myFile, "read" );

            while( NOT fileIsEOF( fileObj ) ){
                
                line = fileReadLine( fileObj );
                
                if (GetToken(line, 1, "=") EQ 'host'){
                    SESSION.host = GetToken(line, 2, "=");
                }
                if (GetToken(line, 1, "=") EQ 'port'){
                    SESSION.port = GetToken(line, 2, "=");
                }
            
                //WriteOutput( "#line#<br />" );
            }
            FileClose( fileObj );

            result =  easySocket('#SESSION.host#','#SESSION.port#', FORM.thisLabel);
            writeoutput(result);
            flush;

        }

               
    }

</cfscript>



<!--- This from here... --->
<!--- http://cflib.org/udf/easySocket --->
<!--- Changed a bit by me to get a reply from the socket... --->
<cffunction name="easySocket" access="private" returntype="any" hint="Uses Java Sockets to connect to a remote socket over TCP/IP" output="false">

    <cfargument name="host" type="string" required="yes" default="localhost" hint="Host to connect to and send the message">
    <cfargument name="port" type="numeric" required="Yes" default="8080" hint="Port to connect to and send the message">
    <cfargument name="message" type="string" required="yes" default="" hint="The message to transmit">

    <cfset var result = "">
    <cfset var socket = createObject( "java", "java.net.Socket" )>
    <cfset var streamOut = "">
    <cfset var output = "">
	<cfset var input = "">

   <cftry>
      <cfset socket.init(arguments.host,arguments.port)>
      <cfcatch type="Object">
         <cfset result = "Could not connected to host <strong>#arguments.host#</strong>, port <strong>#arguments.port#</strong>.">
         <cfreturn result>
      </cfcatch>  
   </cftry>

   <cfif socket.isConnected()>
        <cfscript>
            streamOut = socket.getOutputStream();
            output = createObject("java", "java.io.PrintWriter").init(streamOut);
            streamInput = socket.getInputStream();
            
            inputStreamReader= createObject( "java", "java.io.InputStreamReader").init(streamInput);
            input = createObject( "java", "java.io.BufferedReader").init(InputStreamReader);

            output.println(Trim(arguments.message));
            output.println(chr(13)); 
            output.println();  
            output.flush();

           // if we don't do this, it seems to pick up the input value...
            while ( input.readLine() != '') {
                        result = input.readLine();
                        cfbreak;
                     }

            input.close();
            socket.close();
        </cfscript>
   <cfelse>
      <cfset result = "Could not connected to host <strong>#arguments.host#</strong>, port <strong>#arguments.port#</strong>.">
   </cfif>
   
   <cfreturn result>
</cffunction>